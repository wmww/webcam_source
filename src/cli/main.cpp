#include "pixel_buffer/pixel_buffer_source.h"
#include "pixel_buffer/color_rgb.h"

#include <iostream>
#include <iomanip>

auto const device_path = "/dev/video0";

std::ostream& operator<<(std::ostream& os, ColorRGB const& c)
{
    return os << "#" << std::setfill('0') << std::setw(2) << std::hex << (int)c.r << (int)c.g << (int)c.b;
}

int main(int argc, char** argv)
{
    try {
        std::cout << "Opening " << device_path << std::endl;
        auto input = PixelBufferSource::open_video4linux2_device(device_path, {1280, 720});
        input->open_stream();
        std::cout << "Cam open: " << (input->is_open() ? "yes" : "no") << std::endl;
        std::cout << "Cam resolution: " << input->get_resolution() << std::endl;
        auto buffer = input->get_next_buffer();
        std::cout << "Buffer resolution: " << buffer->get_dimensions() << std::endl;
        std::cout << "First pixel: " << ((ColorRGB*)buffer->get_raw_data())[0] << std::endl;
        std::cout << "Done" << std::endl;
    } catch (std::runtime_error const& e) {
        std::cout << e.what() << std::endl;
    }
}
