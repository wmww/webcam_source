/*#include "util/std.h"
#include "util/error.h"

#include "gtkmm/widget.h"

namespace
{
struct CustomWidget : Gtk::Widget
{
    CustomWidget()
    {
        set_has_window(false);
    }

    Gtk::SizeRequestMode get_request_mode_vfunc() const override // optional
    {
        return Gtk::SIZE_REQUEST_CONSTANT_SIZE;
    }

    void get_preferred_width_vfunc(int& minimum_width, int& natural_width) const override
    {
        minimum_width = 100;
        natural_width = 200;
    }

    void get_preferred_height_for_width_vfunc(int width, int& minimum_height, int& natural_height) const override
    {
        minimum_height = width / 2;
        natural_height = width;
    }

    void get_preferred_height_vfunc(int& minimum_height, int& natural_height) const override
    {
        minimum_height = 100;
        natural_height = 200;
    }

    void get_preferred_width_for_height_vfunc(int height, int& minimum_width, int& natural_width) const override
    {
        minimum_width = height;
        natural_width = height * 2;
    }

    void on_size_allocate(Gtk::Allocation& allocation) override
    {
        Gtk::Widget::on_size_allocate(allocation);
    }

    void on_map() override // optional
    {
        WARNING("Widget on_map()")
        Gtk::Widget::on_map();
    }

    void on_unmap() override // optional
    {
        WARNING("Widget on_unmap()")
        Gtk::Widget::on_unmap();
    }

    void on_realize() override
    {
        WARNING("Widget on_realize()")
        Gtk::Widget::on_realize();
    }

    void on_unrealize() override // optional
    {
        WARNING("Widget on_unrealize()")
        Gtk::Widget::on_unrealize();
    }

    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override
    {
        cr->set_source_rgb(0, 0, 0);
        // printf("BufferStreamWidget::on_draw()\n");
        return true;
    }
};

} // namespace
*/
