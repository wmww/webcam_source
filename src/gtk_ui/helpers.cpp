#include "gtk_ui/gtk_ui.h"
#include "util/std.h"

#include <gdkmm/general.h>

void run_on_gtk_thread(function<void()>&& func)
{
    struct Self
    {
        function<void()> func;
    };

    gdk_threads_add_idle_full(G_PRIORITY_HIGH_IDLE,
                              +[](void* data) -> gboolean {
                                  Self* self = static_cast<Self*>(data);
                                  self->func();
                                  return FALSE;
                              },
                              new Self{func},
                              +[](void* data) {
                                  Self* self = static_cast<Self*>(data);
                                  delete self;
                              });
}
