#include "gtk_ui/gtk_ui.h"
#include "util/error.h"
#include "util/std.h"
#include "gtk_ui/gtk_ui.h"
#include "gl/renderer.h"

#include <gtkmm/application.h>
#include <gtkmm/window.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>

/*
auto make_custom_widget() -> unique_ptr<Gtk::Widget>;

struct MyWindow : Gtk::ApplicationWindow
{
    MyWindow()
        : box(Gtk::ORIENTATION_VERTICAL)
        , button{"Button"}
        , preview_widget{make_gl_widget([](shared_ptr<GlContext> const& context) -> auto { return
Renderer::make_color_renderer(ColorRGB{0, 128, 255}, context); })}
    {
        // usleep(4000000);
        set_title("Viewer");
        set_default_size(800, 600);
        add(box);
        box.pack_start(*preview_widget, Gtk::PACK_SHRINK);
        box.pack_start(button, Gtk::PACK_SHRINK);
        button.signal_clicked().connect(sigc::mem_fun(this, &MyWindow::on_button_quit));
        show_all();
    }

    ~MyWindow()
    {
        // capture_thread.join();
    }

    void on_button_quit()
    {
        close();
        closed = true;
    }

    Gtk::Box box;
    Gtk::Button button;
    bool closed{false};
    unique_ptr<Gtk::Widget> preview_widget;
};

void show_msg(char const* str)
{
    WARNING(str);
}
*/
int main(int argc, char* argv[])
{
    auto app = Gtk::Application::create(argc, argv, "sh.wmww.webcam");
    /*MyWindow window;
    // Gtk::Label label{"Some text"};
    window.signal_realize().connect(sigc::bind(&show_msg, "Window realize"));
    window.signal_map().connect(sigc::bind(&show_msg, "Window map"));
    window.signal_unmap().connect(sigc::bind(&show_msg, "Window unmap"));
    window.signal_unrealize().connect(sigc::bind(&show_msg, "Window unrealize"));
    window.button.signal_realize().connect(sigc::bind(&show_msg, "Label realize"));
    window.button.signal_map().connect(sigc::bind(&show_msg, "Label map"));
    window.button.signal_unmap().connect(sigc::bind(&show_msg, "Label unmap"));
    window.button.signal_unrealize().connect(sigc::bind(&show_msg, "Label unrealize"));
    window.preview_widget->signal_unrealize().connect(sigc::bind(&show_msg, "custom widget unrealized"));
    //window.add(label);*/
    auto window_ = make_viewer_window();
    auto& window = *window_;
    return app->run(window);
}
