#include "gl/shader_program.h"
#include "util/std.h"
#include "util/error.h"

#include <epoxy/gl.h>

namespace
{
struct ShaderImpl
{
    ShaderImpl(string const& code, GLenum type)
        : id{glCreateShader(type)}
    {
        ASSERT(id);
        const char* code_char_ptr = code.c_str();
        glShaderSource(id, 1, &code_char_ptr, nullptr);
        glCompileShader(id);
        GLint success;
        glGetShaderiv(id, GL_COMPILE_STATUS, &success);
        if (!success) {
            static int const buffer_len = 2048;
            GLchar buffer[buffer_len];
            glGetShaderInfoLog(id, buffer_len, nullptr, buffer);
            glDeleteShader(id);
            THROW_ERROR("Shader error: " << buffer);
        }
    }

    ~ShaderImpl()
    {
        glDeleteShader(id);
    }

    GLuint const id;
};

struct ShaderProgramImpl : ShaderProgram
{
    ShaderProgramImpl(vector<shared_ptr<ShaderImpl>> const& shaders)
        : id{glCreateProgram()}
    {
        ASSERT(id);
        for (auto const& shader : shaders) {
            glAttachShader(id, shader->id);
        }
        glLinkProgram(id);
        GLint success;
        glGetProgramiv(id, GL_LINK_STATUS, &success);
        if (!success) {
            static int const buffer_len = 2048;
            GLchar buffer[buffer_len];
            glGetProgramInfoLog(id, buffer_len, nullptr, buffer);
            glDeleteProgram(id);
            THROW_ERROR("Program linking error: " << buffer);
        }
    }

    ~ShaderProgramImpl()
    {
        glDeleteProgram(id);
    }

    void bind_then(function<void()> action) override
    {
        glUseProgram(id);
        action();
        glUseProgram(0);
    }

    void uniform_matrix_f4v(string const& name, float const* data) override
    {
        GLint loc = glGetUniformLocation(id, name.c_str());
        if (loc < 0)
            THROW_ERROR("Invalid uniform name " + name);
        glUniformMatrix4fv(loc, 1, GL_FALSE, data);
    }

    GLuint const id;
};

} // namespace

auto ShaderProgram::make(string const& vertex_code, string const& fragment_code) -> unique_ptr<ShaderProgram>
{
    auto vertex_shader = std::make_shared<ShaderImpl>(vertex_code, GL_VERTEX_SHADER);
    auto fragment_shader = std::make_shared<ShaderImpl>(fragment_code, GL_FRAGMENT_SHADER);
    return make_unique<ShaderProgramImpl>(vector<shared_ptr<ShaderImpl>>{vertex_shader, fragment_shader});
    // It's fine to let the shaders fall out of scope here
}
