#include "gl/renderer.h"
#include "gl/texture.h"
#include "gl/drawable.h"
#include "gl/gl_context.h"
#include "util/std.h"
#include "pixel_buffer/pixel_buffer_source.h"

#include <epoxy/gl.h>

namespace
{
struct TextureRenderer : Renderer
{
    TextureRenderer(shared_ptr<Texture> const& texture, shared_ptr<GlContext> const& context)
        : context{context}
        , texture{texture}
        , drawable{Drawable::make_texture_drawable(texture)}
    {}

    ~TextureRenderer()
    {
        context->make_current_then([this]() {
            texture = nullptr;
            drawable = nullptr;
            ;
        });
    }

    void render() override
    {
        context->make_current_then([this]() {
            glClearColor(0.0, 0.0, 0.1, 1.0);
            glClear(GL_COLOR_BUFFER_BIT);
            drawable->draw();
            glFlush();
        });
    }

    void resize(V2u /*new_size*/) override
    {}

    shared_ptr<GlContext> const context;
    shared_ptr<Texture> texture;
    unique_ptr<Drawable> drawable;
};

} // namespace

auto Renderer::make_texture_renderer(shared_ptr<Texture> const& texture, shared_ptr<GlContext> const& context)
    -> unique_ptr<Renderer>
{
    return make_unique<TextureRenderer>(texture, context);
}
