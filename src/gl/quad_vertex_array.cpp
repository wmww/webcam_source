#include "gl/vertex_array.h"
#include "util/std.h"
#include "util/error.h"

#include <epoxy/gl.h>

namespace
{
struct QuadVertexArray : VertexArray
{
    static auto new_vertex_array_id() -> GLuint
    {
        GLuint id = 0;
        glGenVertexArrays(1, &id);
        ASSERT(id);
        return id;
    }

    QuadVertexArray()
        : id{new_vertex_array_id()}
    {
        GLuint vertex_buffer_id; // VBO
        GLuint element_buffer_id; // EBO

        glGenBuffers(1, &vertex_buffer_id);
        glGenBuffers(1, &element_buffer_id);

        // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
        glBindVertexArray(id);
        {
            glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id);
            {
                glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

                glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
                glEnableVertexAttribArray(0);

                glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
                glEnableVertexAttribArray(1);
            }
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex
            // buffer object so afterwards we can safely unbind

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_buffer_id);
            {
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
            }
            // remember: do NOT unbind the EBO, keep it bound to this VAO
        }
        // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)
        glBindVertexArray(0);

        // this is legal as per https://stackoverflow.com/a/13342549/4327513
        // the buffers will not really be deleted until the vertex array object is deleted
        // its apparently like reference counting, and here we are lowering the reference count
        // I know, OpenGL is fucking stupid
        glDeleteBuffers(1, &vertex_buffer_id);
        glDeleteBuffers(1, &element_buffer_id);
    }

    ~QuadVertexArray()
    {
        glDeleteVertexArrays(1, &id);
    }

    void bind_then(function<void()> action) override
    {
        glBindVertexArray(id);
        action();
        glBindVertexArray(0);
    }

    GLuint const id;

    // clang-format off

    static GLfloat constexpr vertices[]
    {
    //  Position      Texture position
         1.0f,  1.0f,  1.0f,  0.0f, // Top Right
         1.0f, -1.0f,  1.0f,  1.0f, // Bottom Right
        -1.0f, -1.0f,  0.0f,  1.0f, // Bottom Left
        -1.0f,  1.0f,  0.0f,  0.0f, // Top Left
    };

    static GLuint constexpr indices[]
    {
        0, 1, 3, // First Triangle
        1, 2, 3, // Second Triangle
    };

    // clang-format on
};

} // namespace

auto VertexArray::make_quad_vertex_array() -> unique_ptr<VertexArray>
{
    return make_unique<QuadVertexArray>();
}
