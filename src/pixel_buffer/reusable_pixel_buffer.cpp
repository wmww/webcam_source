#include "pixel_buffer/reusable_pixel_buffer.h"
#include "util/std.h"

struct LockedReusableBuffer : PixelBuffer
{
    LockedReusableBuffer(shared_ptr<ReusablePixelBuffer> reusable_buffer)
        : PixelBuffer{reusable_buffer->format}
        , reusable_buffer{reusable_buffer}
    {
        reusable_buffer->lock();
    }

    ~LockedReusableBuffer()
    {
        reusable_buffer->unlock();
    }

    auto get_raw_data() const -> unsigned char* override
    {
        return reusable_buffer->get_raw_data();
    }

    auto get_dimensions() const -> V2u override
    {
        return reusable_buffer->get_dimensions();
    }

    shared_ptr<ReusablePixelBuffer> const reusable_buffer;
};

auto pixel_buffer_from(shared_ptr<ReusablePixelBuffer> reusable_buffer) -> unique_ptr<PixelBuffer>
{
    return std::make_unique<LockedReusableBuffer>(reusable_buffer);
}
