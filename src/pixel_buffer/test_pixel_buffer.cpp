#include "pixel_buffer/pixel_buffer.h"
#include "pixel_buffer/color_rgb.h"
#include "util/std.h"

#include "math.h"

namespace
{
struct TestPixelBuffer : PixelBuffer
{
    TestPixelBuffer(V2u dimensions)
        : PixelBuffer{&PixelFormat::RGB}
        , dimensions{dimensions}
        , data{unique_ptr<ColorRGB[]>(new ColorRGB[dimensions.x * dimensions.y])}
    {}

    auto get_raw_data() const -> unsigned char* override
    {
        return (unsigned char*)data.get();
    }

    auto get_dimensions() const -> V2u override
    {
        return dimensions;
    }

    V2u dimensions;
    unique_ptr<ColorRGB[]> data;
};
} // namespace

auto PixelBuffer::make_test_pixel_buffer(V2u dimensions) -> unique_ptr<PixelBuffer>
{
    auto buffer = std::make_unique<TestPixelBuffer>(dimensions);
    for (auto y = 0u; y < dimensions.y; y++) {
        ColorRGB* row = &buffer->data.get()[y * dimensions.x];
        for (auto x = 0u; x < dimensions.x; x++) {
            ColorRGB* pixel = &row[x];
            double norm_x = ((double)x / dimensions.x) * 2 - 1;
            double norm_y = ((double)y / dimensions.x) * 2 - 1;
            if (sqrt(norm_x * norm_x + norm_y * norm_y) < 0.5) {
                *pixel = ColorRGB{0, 128, 255};
            } else {
                *pixel = ColorRGB{128, 0, 255};
            }
        }
    }
    return buffer;
}
