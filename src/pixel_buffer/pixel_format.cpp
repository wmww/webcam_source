#include "pixel_buffer/pixel_format.h"

PixelFormat const PixelFormat::RGB{3};
PixelFormat const PixelFormat::RGBA{4};
PixelFormat const PixelFormat::BGR{3};
PixelFormat const PixelFormat::BGRA{4};
