#include "pixel_buffer/pixel_buffer_source.h"
#include "pixel_buffer/reusable_pixel_buffer.h"
#include "util/std.h"
#include "util/error.h"
#include "util/lockable_ptr.h"
#include "util/helpers.h"

#include <linux/videodev2.h>
#include <libv4l2.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <mutex>

// Handles opening and getting buffers from a Video4Linux2 device (like /dev/video0)
struct Video4Linux2Device
    : PixelBufferSource
    , SupportsLockablePtr
{
    struct CapturedBuffer;

    // Buffer data created with mmap, which may outlive the video device connection
    struct MMappedBuffer : ReusablePixelBuffer
    {
        MMappedBuffer(unsigned index, unsigned char* data, size_t size, V2u dimensions, Video4Linux2Device* device)
            : ReusablePixelBuffer{&PixelFormat::RGB}
            , index{index}
            , data{data}
            , size{size}
            , dimensions{dimensions}
            , device{device}
        {}

        ~MMappedBuffer()
        {
            v4l2_munmap(data, size);
        }

        void lock() override
        {
            if (is_locked) {
                WARNING("lock() called when already locked");
                return;
            }
            is_locked = true;
        }

        // Called when you are ready for the buffer to be filled/refilled with data
        void unlock() override
        {
            if (!is_locked) {
                WARNING("unlock() called not locked");
                return;
            }
            auto dev = device.lock();
            if (dev) {
                v4l2_buffer buffer;
                set_zero(buffer);
                buffer.index = index;
                buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buffer.memory = V4L2_MEMORY_MMAP;

                dev.value()->persistent_v4l2_ioctl(VIDIOC_QBUF, &buffer);

                is_locked = false;
            }
        }

        auto get_raw_data() const -> unsigned char* override
        {
            return data;
        }

        auto get_dimensions() const -> V2u override
        {
            return dimensions;
        }

        unsigned const index;
        unsigned char* const data;
        size_t const size;
        V2u const dimensions;
        bool is_locked{true}; // It starts out not enqueued, therefor "locked"
        LockablePtr<Video4Linux2Device> device;
    };

    static auto open_device(string const& path) -> unique_ptr<int, std::function<void(int*)>>
    {
        int fd = v4l2_open(path.c_str(), O_RDWR | O_NONBLOCK, 0);
        if (fd < 0) {
            THROW_ERROR("Failed to open " << path << " as a video device");
        } else {
            int* ptr = new int;
            *ptr = fd;
            return unique_ptr<int, std::function<void(int*)>>{ptr, [](int* fd) {
                                                                  v4l2_close(*fd);
                                                                  delete fd;
                                                              }};
        }
    }

    explicit Video4Linux2Device(string const& device_path, V2u requested_resolution)
        : device_path{device_path}
        , device_fd{open_device(device_path)}
        , requested_resolution{requested_resolution}
    {
        // See https://www.linuxtv.org/downloads/v4l-dvb-apis-old/pixfmt.html#v4l2-pix-format for details
        v4l2_format format;
        set_zero(format);
        format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2_pix_format& pix_format = format.fmt.pix;
        pix_format.width = requested_resolution.x;
        pix_format.height = requested_resolution.y;
        pix_format.pixelformat = requested_pixel_format;
        pix_format.field = requested_field;
        pix_format.bytesperline = 0; // "applications can just set this field to zero to get a reasonable default"
        pix_format.sizeimage = 0; // Set by the driver

        persistent_v4l2_ioctl(VIDIOC_S_FMT, &format);

        ASSERT(pix_format.pixelformat == requested_pixel_format);
        ASSERT(pix_format.field == requested_field);
        V2u resolution{pix_format.width, pix_format.height};
        actual_resolution = resolution;
        if (actual_resolution != requested_resolution) {
            WARNING(device_path << " using resolution " << resolution << " instead of " << requested_resolution);
        }

        v4l2_requestbuffers request;
        set_zero(request);
        request.count = 2; // 2 buffers should be enough
        request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        request.memory = V4L2_MEMORY_MMAP;

        persistent_v4l2_ioctl(VIDIOC_REQBUFS, &request);

        ASSERT(request.count == 2);

        ASSERT(buffers.size() == 0); // close_device should have cleared this
        for (auto i = 0u; i < request.count; i++) {
            buffers.push_back(make_mmapped_buffer(i, resolution));
        }
        for (auto& buffer : buffers) {
            buffer->unlock();
        }
    }

    ~Video4Linux2Device()
    {
        invalidate_lockable_ptrs();
        close_stream();
    }

    auto is_open() const -> bool override
    {
        std::lock_guard<std::mutex> lock{mutex};
        return stream_on;
    }

    auto get_resolution() const -> V2u override
    {
        // No mutex lock, because only changes in constructor
        return actual_resolution.value_or(requested_resolution);
    }

    void persistent_v4l2_ioctl(int request, void* data) const
    {
        while (true) {
            int ret = v4l2_ioctl(*device_fd, request, data);
            if (ret == -1) {
                switch (errno) {
                case EINTR:
                case EAGAIN:
                    // Keep looping
                    break;
                default:
                    THROW_ERROR("Could not ioctl " + device_path + ": " + strerror(errno));
                }
            } else {
                return;
            }
        }
    }

    auto make_mmapped_buffer(unsigned index, V2u resolution) -> unique_ptr<MMappedBuffer>
    {
        v4l2_buffer buffer;
        set_zero(buffer);
        buffer.index = index;
        buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

        persistent_v4l2_ioctl(VIDIOC_QUERYBUF, &buffer);

        auto* data = (unsigned char*)
            v4l2_mmap(nullptr, buffer.length, (PROT_READ | PROT_WRITE), MAP_SHARED, *device_fd, buffer.m.offset);

        ASSERT(data);
        ASSERT(data != MAP_FAILED);

        return make_unique<MMappedBuffer>(index, data, buffer.length, resolution, this);
    }

    auto get_next_buffer() -> std::unique_ptr<PixelBuffer> override
    {
        std::lock_guard<std::mutex> lock{mutex};

        v4l2_buffer buffer_info;
        set_zero(buffer_info);
        buffer_info.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buffer_info.memory = V4L2_MEMORY_MMAP;

        persistent_v4l2_ioctl(VIDIOC_DQBUF, &buffer_info);

        optional<shared_ptr<MMappedBuffer>> found;
        for (auto& buffer : buffers) {
            if (buffer->index == buffer_info.index) {
                ASSERT(!found)
                found = buffer;
            }
        }
        ASSERT(found)
        return pixel_buffer_from(found.value());
    }

    void open_stream() override
    {
        std::lock_guard<std::mutex> lock{mutex};

        if (stream_on)
            return;

        v4l2_buf_type buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        persistent_v4l2_ioctl(VIDIOC_STREAMON, &buffer_type);

        stream_on = true;
    }

    void close_stream() override
    {
        std::lock_guard<std::mutex> lock{mutex};

        if (!stream_on)
            return;

        v4l2_buf_type buffer_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        persistent_v4l2_ioctl(VIDIOC_STREAMOFF, &buffer_type);

        stream_on = false;
    }

    string const device_path;
    unique_ptr<int, std::function<void(int*)>> device_fd;
    V2u const requested_resolution;
    optional<V2u> actual_resolution;
    vector<shared_ptr<MMappedBuffer>> buffers;
    __u32 const requested_pixel_format{V4L2_PIX_FMT_RGB24};
    bool stream_on{false};
    v4l2_field const requested_field{
        V4L2_FIELD_NONE}; // We don't want interlacing, see
                          // https://www.linuxtv.org/downloads/v4l-dvb-apis-old/field-order.html#v4l2-field
    std::mutex mutable mutex;
};

auto PixelBufferSource::open_video4linux2_device(string const& device_path, V2u requested_resolution)
    -> unique_ptr<PixelBufferSource>
{
    return make_unique<Video4Linux2Device>(device_path, requested_resolution);
}
