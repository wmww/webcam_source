#ifndef Lockable_PTR_H
#define Lockable_PTR_H

#include <optional>
#include <memory>

template<typename T>
class LockablePtr;

class SupportsLockablePtr;

struct LockablePtrImpl
{
private:
    template<typename T>
    friend class LockablePtr;
    friend class SupportsLockablePtr;
    struct Data;
    static auto lock(Data* data) -> std::optional<void*>;
    // You don't unlock if lock returns nullopt
    static void unlock(Data* data);
};

class SupportsLockablePtr
{
protected:
    SupportsLockablePtr();
    ~SupportsLockablePtr();
    // Should be called at the top of the destructor before any mutexes are locked
    // Can be safely called multiple times (for example at the top of constructors of both a subclass and superclass)
    void invalidate_lockable_ptrs();

private:
    template<typename T>
    friend class LockablePtr;
    std::shared_ptr<LockablePtrImpl::Data> const lockable_ptr_data;
    static auto get_null_data() -> std::shared_ptr<LockablePtrImpl::Data>;
};

template<typename T>
class LockablePtr
{
private:
    std::shared_ptr<LockablePtrImpl::Data> data;
    struct Unlocker
    {
        std::shared_ptr<LockablePtrImpl::Data> const data;
        void operator()(T*)
        {
            LockablePtrImpl::unlock(data.get());
        }
    };

public:
    LockablePtr()
        : data{SupportsLockablePtr::get_null_data()}
    {}

    LockablePtr(T* target)
        : data{static_cast<SupportsLockablePtr*>(target)->lockable_ptr_data}
    {}

    void operator=(T* target)
    {
        data = static_cast<SupportsLockablePtr*>(target)->lockable_ptr_data;
    }

    auto lock() const -> std::optional<std::unique_ptr<T, Unlocker>>
    {
        auto ptr = LockablePtrImpl::lock(data.get());
        if (ptr) {
            auto base_ptr = (SupportsLockablePtr*)ptr.value();
            auto child_ptr = static_cast<T*>(base_ptr);
            return {{child_ptr, Unlocker{data}}};
        } else {
            return std::nullopt;
        }
    }
};

#endif // Lockable_PTR_H
