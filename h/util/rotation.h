#ifndef ROTATION_H
#define ROTATION_H

static double const PI = 3.14159265358979323846;

struct Rotation
{
    explicit inline Rotation(double radians)
        : radians{radians}
    {}

    inline auto operator()() const -> double
    {
        return radians;
    }

    inline auto degrees() const -> double
    {
        return radians * (180.0 / PI);
    }

    inline auto operator-() const -> Rotation
    {
        return Rotation{-radians};
    }

    double radians;
};

inline auto operator"" _radians(long double radians) -> Rotation
{
    return Rotation{(double)radians};
}

inline auto operator"" _degrees(long double degrees) -> Rotation
{
    double radians = degrees * (PI / 180.0);
    return Rotation{radians};
}

inline auto operator"" _degrees(unsigned long long degrees) -> Rotation
{
    double radians = degrees * (PI / 180.0);
    return Rotation{radians};
}

#endif // ROTATION_H
