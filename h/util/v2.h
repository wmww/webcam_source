#ifndef V2_H
#define V2_H

#include <iostream>

template<typename T>
struct V2
{
    T x, y;

    V2()
        : x{T()}
        , y{T()}
    {}

    V2(T x, T y)
        : x{x}
        , y{y}
    {}

    inline auto operator==(V2<T> const& that) const -> bool
    {
        return x == that.x && y == that.y;
    }

    inline auto operator!=(V2<T> const& that) const -> bool
    {
        return !(*this == that);
    }
};

template<typename T>
std::ostream& operator<<(std::ostream& os, V2<T> const& v)
{
    return os << "{" << v.x << ", " << v.y << "}";
}

typedef V2<int> V2i;
typedef V2<unsigned> V2u;
typedef V2<double> V2d;

#endif // V2_H
