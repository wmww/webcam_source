#ifndef ERROR_H
#define ERROR_H

#include <sstream>
#include <iostream>

#define TERM_COLOR(code) "\x1b[" code "m"
#define COLOR_RED TERM_COLOR("1;31")
#define COLOR_YELLOW TERM_COLOR("1;33")
#define COLOR_BLUE TERM_COLOR("34")
#define COLOR_CYAN TERM_COLOR("36")
#define COLOR_NONE TERM_COLOR("0")

#define LINE_CONTEXT COLOR_BLUE __FILE__ COLOR_NONE ":" COLOR_CYAN + std::to_string(__LINE__) + COLOR_NONE ":"

#define THROW_ERROR(message)                                              \
    {                                                                     \
        std::stringstream ss;                                             \
        ss << LINE_CONTEXT << COLOR_RED " ERROR: " COLOR_NONE << message; \
        throw std::runtime_error(ss.str());                               \
    }

#define ASSERT(condition)                                                                               \
    if (!(condition)) {                                                                                 \
        throw std::runtime_error(LINE_CONTEXT + COLOR_RED " assertion failed: " COLOR_NONE #condition); \
    }

#define WARNING(message) std::cout << LINE_CONTEXT << COLOR_YELLOW "WARNING: " COLOR_NONE << message << std::endl;

#define ASSERT_ELSE(condition, action)                                  \
    if (!(condition)) {                                                 \
        WARNING(COLOR_RED " assertion failed: " COLOR_NONE #condition); \
        action;                                                         \
    }

#endif // ERROR_H
