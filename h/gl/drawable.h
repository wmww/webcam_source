#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "util/v2.h"
#include "util/rotation.h"

#include <memory>
using std::shared_ptr;
using std::unique_ptr;

struct Texture;

struct Drawable
{
    static auto make_texture_drawable(shared_ptr<Texture> const& texture) -> unique_ptr<Drawable>;

    Drawable();
    virtual ~Drawable();

    virtual void draw() = 0;

    virtual void set_position(V2d pos);
    virtual void set_size(V2d size);
    virtual void set_rotation(Rotation rotation);
    virtual void set_transform(V2d pos, V2d size, Rotation rotation);

    auto get_position() -> V2d;
    auto get_size() -> V2d;
    auto get_rotation() -> Rotation;
    auto get_glm_mat4_value_ptr() -> float*;

private:
    struct Impl;
    unique_ptr<Impl> impl;
};

#endif // DRAWABLE_H
