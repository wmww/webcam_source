#ifndef GL_CONTEXT_H
#define GL_CONTEXT_H

#include <functional>
using std::function;

struct GlContext
{
    virtual ~GlContext() = default;

    virtual void make_current_then(function<void()> action) = 0;
};

#endif // GL_CONTEXT_H
