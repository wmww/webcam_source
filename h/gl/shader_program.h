#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H

#include <memory>
using std::unique_ptr;

#include <string>
using std::string;

#include <functional>
using std::function;

struct ShaderProgram
{
    static auto make(string const& vertex_code, string const& fragment_code) -> unique_ptr<ShaderProgram>;

    virtual ~ShaderProgram() = default;

    virtual void bind_then(function<void()> action) = 0;

    virtual void uniform_matrix_f4v(string const& name, float const* data) = 0;
};

#endif // SHADER_PROGRAM_H
