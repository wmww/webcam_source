#ifndef REUSABLE_PIXEL_BUFFER_H
#define REUSABLE_PIXEL_BUFFER_H

#include "pixel_buffer.h"

#include <memory>
using std::shared_ptr;
using std::unique_ptr;

// A single underlying mapped buffer, which can be locked into a PixelBuffer. When the pixel buffer is released, the
// reussable buffer can be refilled without being unmapped.

struct LockedReusableBuffer;

struct ReusablePixelBuffer : private PixelBuffer
{
    ReusablePixelBuffer(PixelFormat const* format)
        : PixelBuffer{format}
    {}

    virtual ~ReusablePixelBuffer() = default;

private:
    virtual void lock() = 0;
    virtual void unlock() = 0;

    friend LockedReusableBuffer;
};

auto pixel_buffer_from(shared_ptr<ReusablePixelBuffer> reusable_buffer) -> unique_ptr<PixelBuffer>;

#endif // REUSABLE_PIXEL_BUFFER_H
