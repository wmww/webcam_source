cmake_minimum_required(VERSION 3.1)

project(webcam-loopback LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -Wall -Werror -pthread")

include_directories(h/)

set(common_sources
    src/util/lockable_ptr.cpp
)

set(pixel_buffer_sources
    src/pixel_buffer/pixel_format.cpp
    src/pixel_buffer/reusable_pixel_buffer.cpp
    src/pixel_buffer/test_pixel_buffer.cpp
)

set(video4linux2_sources
    src/video4linux2/video4linux2_device.cpp
)

set(cli_sources
    ${common_sources}
    ${pixel_buffer_sources}
    ${video4linux2_sources}
    src/cli/main.cpp
)

set(gtk_sources
    ${common_sources}
    ${pixel_buffer_sources}
    ${video4linux2_sources}
    src/gtk_ui/main.cpp
    src/gtk_ui/helpers.cpp
    src/gtk_ui/gl_widget.cpp
    src/gtk_ui/viewer_window.cpp
    src/gtk_ui/gtk_gl_context.cpp
    src/gl/drawable.cpp
    src/gl/texture_drawable.cpp
    src/gl/color_renderer.cpp
    src/gl/texture_renderer.cpp
    src/gl/quad_vertex_array.cpp
    src/gl/shader_program.cpp
    src/gl/texture.cpp
)

file(GLOB test_sources
    src/util/lockable_ptr.cpp
    src/test/test.cpp
    test/**.cpp
)

find_package(PkgConfig REQUIRED)

pkg_check_modules(V4L2 REQUIRED libv4l2)
include_directories(${V4L2_INCLUDE_DIRS})
add_definitions(${V4L2_CFLAGS_OTHER})

pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
include_directories(${GTKMM_INCLUDE_DIRS})
add_definitions(${GTKMM_CFLAGS_OTHER})

pkg_check_modules(EPOXY REQUIRED epoxy)
include_directories(${EPOXY_INCLUDE_DIRS})
add_definitions(${EPOXY_CFLAGS_OTHER})

pkg_check_modules(GLM REQUIRED glm)
include_directories(${GLM_INCLUDE_DIRS})

add_executable(webcam-cli ${cli_sources})
add_executable(webcam-gtk ${gtk_sources})
add_executable(test ${test_sources})
target_link_libraries(webcam-cli ${V4L2_LIBRARIES})
target_link_libraries(webcam-gtk ${V4L2_LIBRARIES})
target_link_libraries(webcam-gtk ${GTKMM_LIBRARIES})
target_link_libraries(webcam-gtk ${EPOXY_LIBRARIES})
